CCK allowed values editor
*****************

Description
***********

This module allows users with the appropriate permissions to edit the allowed
values of a CCK field from the node create/edit form itself. This is much more
convenient for the site developers, and as it has its own permissions you can
also open it up to the client*. There are two permissions: users with Edit CCK
allowed values will be able to edit the allowed values of fields which have been
explicitly configured to allow it; users with Edit any CCK allowed values can
edit the allowed values of any appropriate field.

Installation & Use
******************

1. Download and enable the module as usual[1].
2. Set up permissions as you'd like.
3. If you're using the Edit CCK allowed values permission then you need to
   explicitly configure the fields you'd like to be edited by users in that
   role. If you go to the field edit screen you should see a new checkbox titled
   Edit allowed values. Check that and save, and the link should appear on the
   node form for users with the right permission. This setting is configured per
   content type and not per field. 

Warning if opening up to clients
********************************

* Currently this module presents the allowed values box exactly as it appears on
  the field edit form. That means that keys can be specified, and if not the key
  and value will be identical. The ramifications of this could be confusing! For
  example, if someone tries to change the spelling of an allowed value (without
  an explicit key) they might expect that all existing fields will be updated
  but they won't: all existing fields retain their current values, while all new
  nodes would have the updated one.

Known issues/limitations
************************

1. Currently the allowed values aren't validated on submission.
2. I've only tried this with, and opened it up to, optionwidgets_select and
   optionwidgets_buttons.
3. I haven't yet added a theme function for the Edit allowed values link. 


[1] http://drupal.org/documentation/install/modules-themes/modules-5-6

author: AndyF
http://drupal.org/user/220112