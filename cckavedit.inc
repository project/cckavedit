﻿<?php
/**
 * @file
 * Helper functions for CCK allowed values editor.
 */

/**
 * Page callback for allowed values form. Prints the output of drupal_get_form()
 * so that the form doesn't have other page elements rendered as well.
 */
function cckavedit_render_allowed_values_form($type_name, $field_name) {
  print drupal_get_form('cckavedit_allowed_values_form', $type_name, $field_name);
}

/**
 * Stub page callback so that I can use the FAPI to process my allowed values
 * form, and have it issue a redirect to here (the window will close on success
 * anyway, so the output's immaterial). I don't know of a nicer way to do this!
 */
function cckavedit_saved() {
  exit;
}

/**
 * Page callback for Drupal's AHAH system. Prints out JSON encoded data that
 * contains the markup for the form element specified by the arguments.
 *
 * @param
 *   Has a variable number of arguments which are used to identify which FAPI
 *   element to return. If you'd like $form['foo']['bar'] then first argument
 *   would be 'foo' and the second 'bar'.
 */
function cckavedit_ahah() {

  $form = _cckavedit_ahah_helper();
  
  $args = func_get_args();
  $element = $form;
  foreach ($args as $arg) {
    $element = $element[$arg];    
  }
  
  // Unset these as they already exist on the form.
  unset($element['#prefix'], $element['#suffix']);
  
  $output = theme('status_messages');
  $output .= drupal_render($element);
  
  // Final rendering callback.
  drupal_json(array('status' => TRUE, 'data' => $output));
  exit();
}

/**
 * Retrieves the form from the cache and processes it. Ensures that
 * $form['#field_info'] is fresh, and prevents the form from being validated or
 * submitted.
 *
 * @return $form
 *   Fully populated FAPI array for use by the AHAH callback.
 *
 * @see ahah_example_callback_helper
 * @see content_form
 */
function _cckavedit_ahah_helper() {

  // This is needed for node_form().
  module_load_include('inc', 'node', 'node.pages');
  
  $form_state = array('storage' => NULL, 'submitted' => FALSE, 'rebuild' => TRUE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  // Enable the submit/validate handlers to determine whether AHAH-submittted.
  $form_state['ahah_submission'] = TRUE;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  
  // Let's reload the field types, including their allowed values.
  $type = content_types($form['type']['#value']);
  foreach ($type['fields'] as $field_name => $field) {
    $form['#field_info'][$field['field_name']] = $field;
  }
  
  // Evil way of convincing the form builder that this whole form's been
  // validated.
  drupal_validate_form($form['form_id']['#value'], array(), $a = array());
    
  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  return $form;
}

/**
 * Form function for the allowed values form.
 *
 * @see content_field_edit_form
 */
function cckavedit_allowed_values_form(&$form_state, $type_name, $field_name) {

  $field = _cckavedit_get_field($type_name, $field_name);
  
  $field_types = _content_field_types();
  $field_type = $field_types[$field['type']];
  
  $additions = (array) module_invoke($field_type['module'], 'field_settings', 'form', $field);
  
  $form['allowed_values'] = $additions['allowed_values_fieldset']['allowed_values'];
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#attributes' => array('class' => 'cckavedit-allowed-submit'),
  );
  $form['#cckavedit'] = array(
    'type_name' => $type_name,
    'field_name' => $field_name,
  );
  
  $trigger = $field_name . '-cckavedit-trigger';
  $form['#attributes']['onsubmit'] = "return cckavedit_submit_allowed(this, '$trigger');";
  
  return $form;
}

/**
 * Validation handler for the allowed values form.
 */
function cckavedit_allowed_values_form_validate($form, &$form_state) {
}

/**
 * Submission handler for the allowed values form.
 *
 * @see content_field_edit_form_submit
 */
function cckavedit_allowed_values_form_submit($form, &$form_state) {

  module_load_include('inc', 'content', 'includes/content.crud');
  $form_values['allowed_values'] = $form_state['values']['allowed_values'];
  
  // Get the field array so that we don't hose the widget details on
  // submission.
  $field_name = $form['#cckavedit']['field_name'];
  $type_name = $form['#cckavedit']['type_name'];
  $field = _cckavedit_get_field($type_name, $field_name);
  
  $form_values = array_merge($field, $form_values);
  content_field_instance_update($form_values);
  
  $form_state['redirect'] = 'cckavedit/saved';
}

/**
 * Add an 'Edit allowed values' link for the given field to the given form.
 * Performs no checks of its own for whether the link _should_ be added. It's a
 * recursive function.
 *
 * @param $field
 *   The field details in the same format as $form['#field_info'][FIELD_NAME]
 * @param $form
 *   The FAPI array to modify by adding the link.
 * @param $parents
 *   Internal use only - an array of parents to the current elements being
 *   searched through on this iteration.
 *
 * @return
 *   TRUE if it inserted the link; FALSE otherwise
 */
function _cckavedit_add_edit_link($field, &$form, $parents = array()) {

  // Could build up a cache here to speed things up
  
  // Make sure the necessary JS is included.
  _cckavedit_add_js();
  
  $field_name = $field['field_name'];
  foreach (element_children($form) as $key) {
    if ($form[$key]['#field_name'] == $field_name) {
      $path = "cckavedit/allowed/$field[type_name]/$field_name";
      $options = array('attributes' => array('rel' => 'lightmodal'));
      $link = l(t('Edit allowed values'), $path, $options);
      $weight = isset($form[$key]['#weight']) ? $form[$key]['#weight'] + 1 : 1;
      $form[$key]['cckavedit_link'] = array(
      	'#value' => $link,
        '#weight' => $weight,
      );
      
      $wrapper = $field_name . '-cckavedit-widget';
      
      $form[$key]['#prefix'] = '<div id="' . $wrapper . '">';
      $form[$key]['#suffix'] = '</div>';

      // Build the path, which identifies the form element to be replaced.
      $ahah_path = CCKAVEDIT_AHAH_PATH . '/';
      if ($parents) {
        $ahah_path .= implode('/', $parents) . '/';
      }
      $ahah_path .= $key;

      // Add a hidden trigger element. Doesn't seem like the nicest way to do
      // things, but it's simple and effective for the moment.
      $form[$key][cckavedit_trigger] = array(
        '#type' => 'hidden',
        '#value' => '',
        '#prefix' => '<div id="' . $field_name . '-cckavedit-trigger' . '">',
        '#suffix' => '</div>',
        '#ahah' => array(
          'path' => $ahah_path,
          'wrapper' => $wrapper,
          'event' => 'change',
        ),
      );
      
      return TRUE;
    }
    if (_cckavedit_add_edit_link($field, $form[$key], $parents + array($key))) {
      return TRUE;
    }
  }
  
  return FALSE;
}

/**
 * Inserts the necessary JS to allow the allowed values editor to post its data.
 * can be called more than once on the same page request.
 */
function _cckavedit_add_js() {

  static $already_added = FALSE;
  
  $script = <<<EOV
  function cckavedit_submit_allowed(form, trigger) {
    form = $(form);
    $.post(form.attr('action'), form.serialize(), function(data) {
    	$('#' + trigger + ' > input').change();
      Lightbox.end('forceClose');
    });
    return false;
  }
EOV;

  if (!$already_added) {
    drupal_add_js($script, 'inline');
    $already_added = TRUE;
  }
}
