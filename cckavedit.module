<?php
/**
 * @file
 * Allows users with the appropriate permission to edit the allowed values of
 * CCK fields.
 *
 * @todo Currently adds an extra div when replacing the form element.
 * @todo Could add more granular permissions.
 * @todo Don't currently validate. I might be able to get non-numeric keys into
 *       a number field this way for example.
 * @todo See how evil the method I use to avoid the form being validated is in
 *       _cckavedit_ahah_helper().
 * @todo Explore more fully which types of widgets can be edited using this
 *       module, and which can't. See _cckavedit_is_editable().
 * @todo I could add a simple static var cache to _cckavedit_add_edit_link().
 * @todo Add a theme function for the 'Edit allowed values' link.
 */

/**
 * AHAH callback path (without traling slash)
 */
define('CCKAVEDIT_AHAH_PATH', 'cckavedit/ahah');

/**
 * Implements hook_menu().
 */
function cckavedit_menu() {

  $items['cckavedit/allowed/%/%'] = array(
    'title' => 'Edit allowed values',
    'page callback' => 'cckavedit_render_allowed_values_form',
    'page arguments' => array(2, 3),
    'access callback' => 'cckavedit_access',
    'access arguments' => array(2, 3),
    'type' => MENU_CALLBACK,
    'file' => 'cckavedit.inc',
  );
  $items['cckavedit/saved'] = array(
    'title' => 'Saved',
    'page callback' => 'cckavedit_saved',
    'access arguments' => array('Edit CCK allowed values'),
    'type' => MENU_CALLBACK,
    'file' => 'cckavedit.inc',
  );
  $items[CCKAVEDIT_AHAH_PATH] = array(
    'title' => 'AHAH callback',
    'page callback' => 'cckavedit_ahah',
    'access arguments' => array('Edit CCK allowed values'),
    'type' => MENU_CALLBACK,
    'file' => 'cckavedit.inc',
  );
  
  return $items;
}

/**
 * Implements hook_perm().
 */
function cckavedit_perm() {
  return array('Edit CCK allowed values', 'Edit any CCK allowed values');
}

/**
 * Implements hook_widget_settings_alter(). Adds the config options to any
 * appropriate widget settings.
 */
function cckavedit_widget_settings_alter(&$settings, $op, $widget) {
  
  module_load_include('inc', 'cckavedit');
  
  if (_cckavedit_is_editable($widget)) {
    switch ($op) {
      case 'form':
        $settings['cckavedit'] = array(
          '#type' => 'checkbox',
          '#title' => t('Edit allowed values'),
          '#description' => t("Check this box if you'd like users with appropriate permissions to be able to edit the allowed values from the node create/edit form."),
          '#default_value' => isset($widget['cckavedit']) ? $widget['cckavedit'] : FALSE,
        );
        break;
        
      case 'save':
        $settings[] = 'cckavedit';
        break;
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function cckavedit_form_alter(&$form, &$form_state, $form_id) {
    
  if (isset($form['#node']) && ($form['#node']->type . '_node_form' == $form_id)) {
    if (is_array($form['#field_info'])) {
      // Go through the fields and for those that we can edit allowed values we
      // add the link and a hidden element to trigger the AHAH replacement.
      foreach ($form['#field_info'] as $name => $field) {
        if (_cckavedit_can_edit_allowed_values($field)) {
          module_load_include('inc', 'cckavedit');
          _cckavedit_add_edit_link($field, $form);
        }
      }
    }
  }
}

/**
 * Access callback for the edit allowed values form.
 */
function cckavedit_access($type_name, $field_name) {
  $field = _cckavedit_get_field($type_name, $field_name);
  return _cckavedit_can_edit_allowed_values($field);
}

/**
 * Checks if the current user can edit the allowed values for the given field.
 *
 * @param $field
 *   Array of field details (the same format as found in
 *   $form['#field_info'][FIELD_NAME]).
 * 
 * @return
 * 	 TRUE if the user can edit the field's allowed values; FALSE otherwise
 */
function _cckavedit_can_edit_allowed_values($field) {
  
  if (!_cckavedit_is_editable($field['widget'])) {
    return FALSE;
  }
  if (user_access('Edit any CCK allowed values')) {
    return TRUE;
  }
  if ($field['widget']['cckavedit'] && user_access('Edit CCK allowed values')) {
    return TRUE;
  }
  
  return FALSE;
}

/**
 * Checks if it's possible in principle to edit the allowed values of the given
 * widget with this module.
 *
 * @param $widget
 *   The widget array for the field; will accept both the format found in
 *   $form['#field_info'][FIELD_NAME]['widget'] and the format returned from
 *   hook_widget_settings_alter() when $op == 'save'.
 * 
 * @return boolean
 *   TRUE if the field has allowed values that can be edited by this module;
 *   FALSE otherwise.
 */
function _cckavedit_is_editable($widget) {

  $allowed_types = array('optionwidgets_select', 'optionwidgets_buttons');
  
  $type = isset($widget['widget_type']) ? $widget['widget_type'] : $widget['type'];
  $ret = $type && in_array($type, $allowed_types);
  return $ret;
}

/**
 * Given a content type and field name, returns the associated field array.
 * 
 * @param $type_name
 *   Content type
 * @param $field_name
 *   Name of field
 *
 * @return
 *   The field array for the specified field
 */
function _cckavedit_get_field($type_name, $field_name) {
  $type = content_types($type_name);
  return $type['fields'][$field_name];
}
